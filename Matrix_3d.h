//
// Created by Stephen Lowinger on 2/23/17.
//

#ifndef MATRIX_3D_H
#define MATRIX_3D_H
#include <vector>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <cmath>

using std::vector;
using std::endl;
using std::ostream;
using std::streamsize;
using std::setw;
using std::pow;

namespace heat_3d_simulation {
#define X 0
#define Y 1

    template<class T>
    class Matrix_3D {
    public:
        Matrix_3D(int size) :
                data(pow(size, 3)), length(size), c(size * size), r(size * size * size),
                value_count((size - 2) * (size - 2) * (size - 2)) {}

        T &operator()(int x, int y, int z) {
            int index = row(x) + column(y) + z;
            return data[index];
        }

        T operator()(int x, int y, int z) const {
            int index = row(x) + column(y) + z;
            return data[index];
        }

        int size() const {
            return length;
        }

        T local_matrix_total() const {
            return value_total;
        }

        double variance_sum(T mean, int mype) const {
            T sum = 0;

            for (int i = 1; i < length - 1; ++i) {
                for (int j = 1; j < length - 1; ++j) {
                    for (int k = 1; k < length - 1; ++k) {
                        double s = ((*this)(i, j, k) - mean);
                        sum += pow(((*this)(i, j, k) - mean), 2);
                    }
                }
            }
            return sum;
        }

        inline friend
        ostream &operator<<(ostream &os, const Matrix_3D<T> &m) {
            int size = m.size();
            os << std::fixed;
            for (int i = 1; i < size - 1; i++) {
                for (int j = 1; j < size - 1; j++) {
                    os << std::setprecision(5) << m(i, j, size - 4) << " ";
                }
                os << endl;
            }
            return os;
        }

//        inline friend
//        ostream &operator<<(ostream &os, const Matrix_3D<T> &m) {
//            int size = m.size();
//            os << "[ " << endl;
//            for (int i = 0; i < size; i++) {
//                for (int j = 0; j < size; j++) {
//                    for (int k = 0; k < size; k++) {
//
//                        os << m(i, j, k) << " ";
//                    }
//                    os << endl;
//                }
//                os << endl;
//            }
//            os << "]" << endl;
//            return os;
//        }

        vector<T> get_plane(int dimension, bool forward, bool include_buffer) {
            if (dimension == X) {
                int index;
                if (forward)
                    index = length - 2;
                else
                    index = 1;

                auto begin = data.begin() + row(index);
                auto end = begin + c;
                return vector<T>(begin, end);

            } else if (dimension == Y) {
                vector<T> out;
                int index;
                if (forward)
                    index = length - 2;
                else
                    index = 1;

                for (int i = 0; i < length; ++i) {
                    int start = row(i) + column(index);
                    int last = start + length;
                    out.insert(out.end(), data.begin() + start, data.begin() + last);
                }
                return out;

            } else {
                vector<T> out;
                int index;
                int start, end;

                if (forward)
                    index = length - 2;
                else
                    index = 1;

                if (include_buffer) {
                    out.resize(c);
                    start = 0;
                    end = length;
                } else {
                    out.resize((length-2)*(length-2));
                    start = 1;
                    end = length-1;
                }

                for (int i = start; i < end; ++i) {
                    for (int j = start; j < end; ++j) {

                        int col;
                        int val;
                        if (include_buffer){
                            col = i;
                            val = j;
                        } else {
                            col = i-1;
                            val = j-1;
                        }
                        if (include_buffer)
                            out[column(i) + j] = data[row(i) + column(j) + index];
                        else
                            out[(length-2)*col + val] = data[row(i) + column(j) + index];
                    }
                }
                return out;
            }
        }

        void receive_plane(const vector<T> &plane, int dimension, bool forward) {
            int index;
            if (forward)
                index = length - 1;
            else
                index = 0;
            int count = 0;

            if (dimension == X) {
                for (int j = 0; j < length; ++j) {
                    for (int k = 0; k < length; ++k) {
                        (*this)(index, j, k) = plane[count];
                        count++;
                    }
                }
            } else if (dimension == Y) {
                for (int i = 0; i < length; ++i) {
                    for (int k = 0; k < length; ++k) {
                        (*this)(i, index, k) = plane[count];
                        count++;
                    }
                }
            } else {
                for (int i = 0; i < length; ++i) {
                    for (int j = 0; j < length; ++j) {
                        (*this)(i, j, index) = plane[count];
                        count++;
                    }
                }
            }
        }

        static
        Matrix_3D<T> heat3d(Matrix_3D<T> const &temp_in, T alpha, T delta_time, T delta);

        static
        Matrix_3D<T>
        gauss3d(T x_min, T y_min, T z_min, int size, T dx, T variance, T mean);

    private:
        int length;
        int c;
        int r;

        T value_total;
        int value_count;

        vector<T> data;


        int row(int x) const {
            return length * length * x;
        }

        int column(int y) const {
            return length * y;
        }
    };
}

#endif //MATRIX_3D_H

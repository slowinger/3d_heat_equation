

#ifndef HEAT_3D_SIMULATION_H
#define HEAT_3D_SIMULATION_H
#include <vector>
#include <cmath>
#include "Matrix_3d.h"
#include <mpi.h>
#include <iostream>
#include <fstream>

#define MAX_VALUE 2
#define BOUNDARY_TEMP 0
#define EXCH 0

using std::vector;
using std::string;

namespace heat_3d_simulation {

    template<>
    Matrix_3D<double> Matrix_3D<double >::heat3d(Matrix_3D<double> const &temp_in, double alpha, double delta_time, double delta) {
        int size = temp_in.size();
        Matrix_3D<double> temp_out(size);

        temp_out.value_total = 0;
        for (int i = 0; i < size; ++i) {
            for (int j = 0; j < size; ++j) {
                for (int k = 0; k < size; ++k) {
                    if (i == 0 || j == 0 || k == 0 || i == size-1 || j == size-1 || k == size -1) {
                        temp_out(i, j, k) = temp_in(i, j, k);
                    } else {
                        temp_out(i,j,k) = temp_in(i,j,k) +
                                          ((alpha * delta_time) / pow(delta, 2))
                                          * (temp_in(i-1,j,k) + temp_in(i+1,j,k) + temp_in(i,j-1,k) + temp_in(i,j+1,k)
                                             + temp_in(i,j,k-1) + temp_in(i,j,k+1) - 6*temp_in(i,j,k));
                        temp_out.value_total += temp_out(i,j,k);
                   }
                }
            }
        }
        return temp_out;
    }

    template<>
    Matrix_3D<double> Matrix_3D<double >::gauss3d(double x_min, double y_min, double z_min, int size, double dx, double variance, double mean) {
        const double c_1 = 1 / pow(2 * M_PI * variance,1.5);
        //Size of matrix
        Matrix_3D<double > out(size+2);

        for (int i = 0; i < size+2; ++i) {
            for (int j = 0; j < size+2; ++j) {
                for (int k = 0; k < size+2; ++k) {
                    if ((i  == 0 || j == 0 || k == 0) || (i  == size+1 || j == size+1 || k == size+1)) {
                        /*
                         * Inner matrices these are not a boundary, but cell
                         * will be overwritten after ghost transfer.
                         * Therefore simpler to just set all to boundary temp
                         */
                        out(i,j,k) = BOUNDARY_TEMP;
                    } else {
                        double temp = c_1 / exp((( pow((x_min + i*dx) - mean, 2) + pow((y_min + j*dx) - mean, 2) +
                                                   pow((z_min + k*dx) - mean, 2))) / ( 2 * variance));
                        out(i,j,k)=temp;
                        out.value_total += temp;
                    }
                }
            }
        }
        return out;
    }

    void write_output(const char *file_name, const vector<double > &data, int n, int n_local, const vector<int> &start) {
        MPI_Datatype matrix_2d;
        vector<int> global_size = { n, n};
        vector<int> local_size = { n_local, n_local};
        int l_size = n_local * n_local;
        MPI_Status status;
        MPI_File   file_out;

        MPI_Type_create_subarray(2, &global_size[0], &local_size[0], &start[0], MPI_ORDER_C, MPI_DOUBLE, &matrix_2d);
        MPI_Type_commit(&matrix_2d);

        MPI_File_open(MPI_COMM_SELF, file_name, MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &file_out);
        MPI_File_set_view(file_out,0, MPI_DOUBLE, matrix_2d, "native", MPI_INFO_NULL);
        MPI_File_write_all(file_out, &(data[0]), l_size, MPI_DOUBLE, &status);
        MPI_File_close(&file_out);

        MPI_Type_free(&matrix_2d);
    }

    int exchange_ghost_cells(int neighbor, int dim, bool forward, int exchange_size, const vector<int> &neighbors,
                             vector<MPI_Request> &requests, vector<vector<double>> &in,
                             Matrix_3D<double > &temperature) {

        if (neighbors[neighbor] >= 0) {
            MPI_Request send, recieve;
            requests.push_back(send);

            MPI_Irecv(&in[neighbor][0], exchange_size, MPI_DOUBLE, neighbors[neighbor], EXCH, MPI_COMM_WORLD, &requests.back());
            requests.push_back(recieve);

            MPI_Isend(&temperature.get_plane(dim, forward, true)[0], exchange_size, MPI_DOUBLE, neighbors[neighbor], EXCH, MPI_COMM_WORLD, &requests.back());
            return 2;
        }
        return 0;

    }

}



#endif

cmake_minimum_required(VERSION 3.6)
project(mpi_performance)

set(CMAKE_CXX_STANDARD 14)

find_package(MPI REQUIRED)

include_directories(${MPI_INCLUDE_PATH})

set(SOURCE_FILES main.cpp heat_3d_simulation.h Matrix_3d.h)
add_executable(3d_heat_equation ${SOURCE_FILES})
target_link_libraries(3d_heat_equation ${MPI_LIBRARIES})

if(MPI_COMPILE_FLAGS)
    set_target_properties(untitled PROPERTIES
            COMPILE_FLAGS "${MPI_COMPILE_FLAGS}")
endif()

if(MPI_LINK_FLAGS)
    set_target_properties(untitled PROPERTIES
            LINK_FLAGS "${MPI_LINK_FLAGS}")
endif()
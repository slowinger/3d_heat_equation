
#include <iostream>
#include <vector>
#include <cmath>
#include <numeric>
#include <fstream>
#include <chrono>
#include <mpi.h>
#include "heat_3d_simulation.h"

#define BROADCASTER 0
#define DIM 3
#define ALPHA .000097
#define EXCH 0
#define FILE_OUTPUT_PATH "/Users/slowinger/CLionProjects/3d_heat_equation/output_files/"


using namespace std;
using namespace heat_3d_simulation;

int main(int argc, char **argv) {
    const double var = 0.2, mean = 1;               //GAUSSIAN DISTRIBUTION ASSUMPTIONS
    const double max = MAX_VALUE;                   //Max bounds of matrix
    const int reorder=1, periods[DIM]={0};          //Cartesian Coordinate inputs

    int nprocs, mype, n, n_local, n_total, coord_dim, n_ghost_vector, t_output, step=0, file_count=0, exchange_count = 0;;
    int coords[DIM], neighbor_coords[DIM], dim[DIM];
    vector<int> neighbors(6);

    dim[0] = dim[1] = dim[2] = 0;
    double delta, delta_time, time_steps_total, time_step = 0;
    double x_start, y_start, z_start, local_sum_mean, local_sum_variance, temp_mean, temp_standard_dev;
    vector<double> local_sums_mean, local_sums_variance;

    clock_t start, end;

    ofstream summary_file;

    MPI_Comm cartesian_comm;

    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &nprocs);
    MPI_Comm_rank(MPI_COMM_WORLD, &mype);

    /*
     * Assumes nprocs^(1/3) is an int
     * Also max is divisble by coord_dim
     */
    if (mype == BROADCASTER) {
        n = atoi(argv[1]);
        delta_time = atof(argv[2]);
        time_steps_total = atof(argv[3]);
        t_output = atoi(argv[4]);
        n_total = n * n * n;
        start = clock();
        cout << " running with " << nprocs << endl;
    }


    MPI_Bcast(&n, 1, MPI_INT, BROADCASTER, MPI_COMM_WORLD);
    MPI_Bcast(&delta_time, 1, MPI_DOUBLE, BROADCASTER, MPI_COMM_WORLD);
    MPI_Bcast(&time_steps_total, 1, MPI_DOUBLE, BROADCASTER, MPI_COMM_WORLD);
    MPI_Bcast(&t_output, 1, MPI_INT, BROADCASTER, MPI_COMM_WORLD);


    delta = max / n;
    if (mype == BROADCASTER){

        double stability_condition = (ALPHA * delta_time)/pow(delta, 3);
        cout << "stability condition is " << stability_condition << endl;
    }

    if ((ALPHA * delta_time)/pow(delta, 3) < 1/8.) {

        coord_dim = (int) cbrt(nprocs);

        n_local = (int) (n / coord_dim);
        n_ghost_vector = (n_local + 2) * (n_local + 2);

        Matrix_3D<double> temperature(n_local), temperature_next(n_local);

        MPI_Dims_create(nprocs, 3, dim);
        MPI_Cart_create(MPI_COMM_WORLD, 3, dim, periods, reorder, &cartesian_comm);

        MPI_Cart_coords(cartesian_comm, mype, 3, coords);

        x_start =  (double) coords[0] / (double) coord_dim * max;
        y_start =  (double) coords[1] / (double) coord_dim * max;
        z_start =  (double) coords[2] / (double) coord_dim * max;

        MPI_Barrier(MPI_COMM_WORLD);

        MPI_Cart_shift(cartesian_comm, 0, 1, &neighbors[0], &neighbors[1]);
        MPI_Cart_shift(cartesian_comm, 1, 1, &neighbors[2], &neighbors[3]);
        MPI_Cart_shift(cartesian_comm, 2, 1, &neighbors[4], &neighbors[5]);

        while (time_step < time_steps_total) {
            step++;
            if (time_step > 0.00001) {
                temperature_next = Matrix_3D<double>::heat3d(temperature, ALPHA, delta_time, delta);
            } else
                temperature_next = Matrix_3D<double>::gauss3d(x_start, y_start, z_start, n_local, delta, var, mean);

            vector<MPI_Request> requests;
            vector<vector<double>> ghost_vectors_in(6, vector<double>(n_ghost_vector));


            /// Do ghost cell exchanges
            for (int i = 0; i < neighbors.size(); ++i) {
                if (neighbors[i] >= 0) {
                    int exchange_dim;
                    int diff;

                    MPI_Cart_coords(cartesian_comm, neighbors[i], 3, neighbor_coords);

                    if (neighbor_coords[0] != coords[0]){
                        exchange_dim = 0;
                        diff = neighbor_coords[0] - coords[0];
                    } else if (neighbor_coords[1] != coords[1]) {
                        exchange_dim = 1;
                        diff = neighbor_coords[1] - coords[1];
                    } else {
                        exchange_dim = 2;
                        diff = neighbor_coords[2] - coords[2];
                    }

                    bool forward = false;
                    if (diff == 1)
                        forward = true;

                    exchange_count += exchange_ghost_cells(i, exchange_dim, forward, n_ghost_vector, neighbors, requests,
                                                           ghost_vectors_in, temperature_next);
                }
            }

            MPI_Status statuses[exchange_count];

            MPI_Waitall(requests.size(), &requests[0], statuses);

            for (int j = 0; j < neighbors.size(); ++j) {
                if (neighbors[j] >= 0) {
                    int exchange_dim;
                    int diff;

                    MPI_Cart_coords(cartesian_comm, neighbors[j], 3, neighbor_coords);

                    if (neighbor_coords[0] != coords[0]){
                        exchange_dim = 0;
                        diff = neighbor_coords[0] - coords[0];
                    } else if (neighbor_coords[1] != coords[1]) {
                        exchange_dim = 1;
                        diff = neighbor_coords[1] - coords[1];
                    } else {
                        exchange_dim = 2;
                        diff = neighbor_coords[2] - coords[2];
                    }

                    bool forward = false;
                    if (diff == 1)
                        forward = true;

//                    if (exchange_dim==2) {
//                        if (mype==BROADCASTER) {
//                            cout << "-----BEFORE EXCHANGE dim " << exchange_dim << endl;
//                            cout << temperature_next << endl;
//                        }
//                    }


                    temperature_next.receive_plane(ghost_vectors_in[j], exchange_dim, forward);
//                    if (exchange_dim==2) {
//                        if (mype==BROADCASTER) {
//                            cout << "-----AFTER EXCHANGE" << time_step <<  endl;
//                            cout << temperature_next << endl;
//                        }
//                    }

                }

            }
//            if (mype==BROADCASTER) {
//                cout << "-----AFTER EXCHANGE" << time_step <<  endl;
//                cout << temperature_next << endl;
//            }

            temperature = temperature_next;


            if (step % t_output == 0){
                /// Compute mean and standard deviation
                local_sum_mean = temperature.local_matrix_total();

                if (mype == BROADCASTER) {
                    local_sums_mean.resize(nprocs);
                    local_sums_variance.resize(nprocs);
                }


                MPI_Gather(&local_sum_mean, 1, MPI_DOUBLE,
                           &local_sums_mean[0], 1, MPI_DOUBLE, BROADCASTER, MPI_COMM_WORLD);

                //Compute Mean
                if (mype == BROADCASTER) {
                    double sum = accumulate(local_sums_mean.begin(), local_sums_mean.end(), 0.0);
                    temp_mean = sum / n_total;
                    cout << time_step << " mean " << temp_mean << endl;
                }

                MPI_Bcast(&temp_mean, 1, MPI_DOUBLE, BROADCASTER, MPI_COMM_WORLD);
                local_sum_variance = temperature.variance_sum(temp_mean, mype);
                MPI_Gather(&local_sum_variance, 1, MPI_DOUBLE,
                           &local_sums_variance[0], 1, MPI_DOUBLE, BROADCASTER, MPI_COMM_WORLD);

                if (mype == BROADCASTER) {
                    double sum = accumulate(local_sums_variance.begin(), local_sums_variance.end(), 0.0);
                    temp_standard_dev = sqrt(sum / n_total);
                    cout << time_step << " standard deviation " << temp_standard_dev << endl;
                    ostringstream file_name;
                    file_name << FILE_OUTPUT_PATH << "3d_heat_transfer.summary";
                    cout << mype << " writing summary data to" << file_name.str() << endl;
                    summary_file.open(file_name.str(), std::ios::out | std::ios::app);
                    summary_file << time_step << "," << temp_mean  << "," << temp_standard_dev << endl;
                    summary_file.close();
                }

                /// Writing to output file
                if (coords[2] == coord_dim/2) {
                    std::ostringstream file;
                    int x_start_index = (coords[0]*n)/coord_dim;
                    int y_start_index = (coords[1]*n)/coord_dim;
                    vector<int> start = {x_start_index, y_start_index};

                    file << FILE_OUTPUT_PATH << "time_step_" << file_count
                         << ".dat";
                    auto temp_out = temperature.get_plane(2, true, false);
                    cout << mype << " writing matrix of size " << temp_out.size()
                         << " to " << file.str() << endl;

                    write_output(file.str().c_str(), temp_out, n, n_local, start);
                    file_count++;
                }

            }
            time_step += delta_time;
        }

    } else
        cout << "Stability condition not met" << endl;

    MPI_Barrier(MPI_COMM_WORLD);
    if (mype==BROADCASTER) {
        end = clock();
        cout << "FINISHED: total time for " << nprocs << " processes: " << (end - start) / (double)(CLOCKS_PER_SEC / 1000) << " ms" << endl;
    }

    MPI_Finalize();
    return 0;
}


